package com.notmythingy.dev.notes

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.HandleBeforeCreate
import org.springframework.data.rest.core.annotation.RepositoryEventHandler
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@SpringBootApplication
class NotesApplication

fun main(args: Array<String>) {
    runApplication<NotesApplication>(*args)
}


@Entity
data class Note(@Id @GeneratedValue var id: Long? = null,
                var title: String? = null,
                var text: String? = null,
                @JsonIgnore var username: String? = null)


@RepositoryRestResource
interface NotesRepository : JpaRepository<Note, Long> {
    fun findAllByUsername(name: String): List<Note>
}


@Component
@RepositoryEventHandler(Note::class)
class AddUserToNote {

    @HandleBeforeCreate
    fun handleCreate(note: Note) {
        val username: String = SecurityContextHolder.getContext().authentication.name
        note.username = username
        println("Creating note: $note")
    }
}


@Component
class DataInitializer(val repository: NotesRepository) : ApplicationRunner {

    override fun run(args: ApplicationArguments?) {
        listOf("Note 1", "Note 2", "Note 3").forEach {
            repository.save(Note(title = it, username = "user"))
        }
        repository.findAll().forEach { println(it) }
    }
}